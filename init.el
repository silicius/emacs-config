;; use lexical bindings in this file (only) from now on
(setq lexical-binding t)

;; start the server as soon as possible
(server-start)

;; Initiliaze package funcionality with MELPA archive
(require 'package)
(add-to-list 'package-archives
	     `("MELPA" . "https://melpa.org/packages/") t)

(package-initialize)

;; install use-package if not present
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; helper macro to define file and directory constants
(defmacro c/defconst-file (file-name file-name-docstring
                           file      file-docstring
                                     file-params)
  `(progn
     (defconst ,file-name ,(car file-params)
       ,file-name-docstring)
     (defconst ,file (expand-file-name ,file-name ,(cdr file-params))
       ,file-docstring)))

;; constants
;; "c/" is the prefix to use with my function and variables related to this config


(c/defconst-file c/config-dir-name "A folder to put my dirs in."
                 c/config-dir      "Here be dragons."
                 ("config/" . user-emacs-directory))
(make-directory c/config-dir t)

(c/defconst-file c/cache-dir-name "The almost-trash bin of files."
                 c/cache-dir      "Almost useless junk here."
                ("cache/" . c/config-dir))
(make-directory c/cache-dir t)

;; configure backup behaviour
(c/defconst-file c/backup-dir-name "Backup file are here."
                 c/backup-dir      "Stop littering my filesystem!"
                 ("backup/" . c/cache-dir))


(setq backup-directory-alist `(("." . ,c/backup-dir)) ; set the backup dir for all files 
      make-backup-files    t  ; enable backups
      vc-make-backup-files t  ; make backups for verion controlled files too
      version-control      t  ; enable numbered backups

      kept-old-versions    10 ; keep 10 oldest backups for each file
      kept-new-versions    10 ; keep 10 newest --||--
      delete-old-versions  t  ; delete excess backups silently

      backup-by-copying    t  ; copy on backup
      )


(c/defconst-file c/auto-save-dir-name "Auto save directory name"
                 c/auto-save-dir      "and dir"
                 ("auto-save/" . c/cache-dir))

(setq auto-save-timeout 30       ; 30 seconds of idle triggers autosave
      auto-save-list-file-prefix c/auto-save-dir
      auto-save-list-file-name   c/auto-save-dir

      auto-save-file-name-transforms `((".*" ,c/auto-save-dir t))
 )

;; disables bell sound and flashes screen instead
(setq visible-bell t)


;; disable features using a negative value
(scroll-bar-mode -1)                ; vertical   scrollbar
(horizontal-scroll-bar-mode -1)     ; horizontal scrollbar
(menu-bar-mode -1)                  ; menu bar
(tool-bar-mode -1)                  ; toolbar
(tooltip-mode -1)                   ; will show tooltips in minbuffer instead


;;disable all dialog boxes (graphical) and use minibuffer instead
(setq use-dialog-box nil)


;; replaces all yes/no with y/n by aliasing it
;; both symbols are in fact functions
(defalias 'yes-or-no-p 'y-or-n-p)

;; save the minibuffer's history
(c/defconst-file c/savehist-file-name "savehist filename"
		 c/savehist-file      "all minibuffer history goes here."
		 ("minibuffer-history" . c/cache-dir))

(setq savehist-save-minibuffer-history t    ; save all minibuffer histories
      savehist-file c/savehist-file         ; it's default save location
      history-length t                      ; unlimited history length
      history-delete-duplicates t           ; as the name suggest
      ) 
(savehist-mode t) ; enable the mode

(use-package paredit
  :ensure t
  :init
  (add-hook 'prog-mode-hook       'enable-paredit-mode)
  (add-hook 'scheme-mode-hook     'enable-paredit-mode)
  (add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
  (add-hook 'lisp-mode-hook       'enable-paredit-mode)
  (add-hook 'paredit-mode-hook (lambda ()
				 (show-paren-mode))))

(use-package geiser
  :ensure t
  :init
  (c/defconst-file c/geiser-dir-name "Geiser's directory name"
		   c/geiser-dir      "Geiser's directory"
		   ("geiser/" . c/cache-dir))
  (make-directory c/geiser-dir t)

  (setq geiser-active-implementations '(chicken guile)
	geiser-repl-history-filename (expand-file-name "history" c/geiser-dir-name)
	)

  (add-hook 'scheme-mode-hook 'turn-on-geiser-mode))

(use-package telega
  :ensure t
  :defer t
  :init
  (c/defconst-file c/telega-dir-name "nobody reads this"
		   c/telega-dir      "it's useless, but I couldn't take time to write a better macro..."
		   ("telega/" . c/cache-dir))
  (setq telega-directory c/telega-dir)
  )

(use-package magit
  :ensure t)
